<?php 
require('vendor/autoload.php');
require('Logger.php');
require('Finggerprint.php');
$index = new Index();
if(count($argv) > 0) {
  foreach ($argv as $key => $value) {
    if($key > 0) {
      $index->run($value);
    }
  }
}

class Index {
  protected $config = [];
  public $url = 'http://159.65.6.236/v1/';
  public $raspbery = [];
  public $fingerprints = [];

  public function run($action) {
    $this->$action();
  }

  
  public function reportEnv() {
    echo ('Reporting Current ENV'); 
    $client = new GuzzleHttp\Client();
    $res = $client->post($this->url.'setting/create', [
      'form_params' => [
        'data' => json_encode([
          'config' => 'CURRENTRASPBERRYPIIP',
          'value' => $this->getIp()
        ])
      ],
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded'
      ]
    ]);
  }
  public function attendaceSync() {
    echo ('Reporting Current ENV'); 
    $client = new GuzzleHttp\Client();
    $res = $client->post($this->url.'attendance/check');
  }
  public function getConfig() {
    echo "Getting Config From Server \n";
    $client = new GuzzleHttp\Client();
    $res = $client->post($this->url.'setting/get-by-config', [
      'form_params' => [
        'data' => json_encode([
          'config' => 'RASPBERRYIP'
        ])
      ],
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded'
      ]
    ]);
    $this->raspbery = json_decode($res->getBody());
  }
  public function getFingerprints() {
    echo "Getting Config From Server \n";
    $client = new GuzzleHttp\Client();
    $res = $client->post($this->url.'setting/get-by-config', [
      'form_params' => [
        'data' => json_encode([
          'config' => 'FINGGERPRINTIP'
        ])
      ],
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded'
      ]
    ]);
    $this->fingerprints = json_decode($res->getBody());
    $this->fingerptints = json_decode($this->fingerprints->data[0]->value);
  }
  public function getAbsensiData() {
    $finggerprintData = [];
    foreach ($this->fingerptints->ips as $key => $fingerprint) {
      $finggerprintData = Finggerprint::getData($fingerprint, '0');
    }
    // $finggerprintData =  json_encode($finggerprintData);
    $client = new GuzzleHttp\Client();
    $container = [];
    $history = GuzzleHttp\Middleware::history($container);
    $requestDetail = json_encode([
      'syncdata' => $finggerprintData
    ]);
    // var_dump($requestDetail);  
    $myfile = fopen("./jsonFile.txt", "w") or die("Unable to open file!");
    fwrite($myfile, $requestDetail);
    fclose($myfile);

    $this->splitData($client);
    
//    var_dump(json_decode($res->getBody()));
    $this->fingerptintsData = json_encode($finggerprintData);
    
  }
  public function splitData($client) {

    $myfile = fopen("./jsonFile.txt", "r") or die("Unable to open file!");
    $lines = fread($myfile,filesize("./jsonFile.txt"));
    $lines = (json_decode($lines));
    $lines = $lines->syncdata;
    $lineCount = (int) (count($lines) / 1000);
    $lineCount = $lineCount + 1;
    for ($i=0; $i < $lineCount; $i++) { 
      # code...
    
      echo("SENDING KE : " . $i . "\n");
      $startLine = $i * 1000;
      
      $endLine =  $startLine + 1000;
      $fingerprintData = array_slice($lines,$startLine,1000);

      // for ($sendLine=$startLine; $sendLine < $endLine; $sendLine++) { 
      //   # code...
      //   // array_push($fingerprintData, $lines[$sendLine]);
      // }
      $size = count($fingerprintData);
      // echo(" $startLine Start $endLine Size $size\n");
      // echo("$i\n");


      if($size > 0 ) {
        $requestData = json_encode([
          'syncdata' => $fingerprintData
        ]);
        
        $this->sendData($client, $requestData);
      }
    }
  }

  private function sendData($client, $requestDetail) {

    // var_dump($requestDetail);
    $res = $client->post($this->url.'raspattendance/sync', [
      'form_params' => [
        'data' => $requestDetail
      ],
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded'
      ]
    ]);
    // var_dump($res);
    return $res;

  }
  public function cleaningUpLastWeekAbsensi() {

     $finggerprintData = [];
     foreach ($this->fingerptints->ips as $key => $fingerprint) {
       $finggerprintData = Finggerprint::clearData($fingerprint, '0');
     }
  }
  function getIp()
  {
    $result = '';
    exec('/sbin/ifconfig eth0|grep "inet"', $resultArray);
    $result = $result . implode(",", $resultArray);
    exec('/sbin/ifconfig wlan0|grep "inet"', $resultArray);
    $result = $result . implode(",", $resultArray);
    exec('/sbin/ifconfig en0|grep "inet"', $resultArray);
    $result = $result . implode(",", $resultArray);
    preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/", $result, $matches);
    return (join(" ",$matches));
  }
}
?>
