
<?php 
class Finggerprint {
  
  public static function getData($ip, $key)
  {
    $Connect = fsockopen($ip, "80", $errno, $errstr, 1);
    if($Connect){
      $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
      $newLine="\r\n";
      fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
        fputs($Connect, "Content-Type: text/xml".$newLine);
        fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
        fputs($Connect, $soap_request.$newLine);
      $buffer="";
      while($Response=fgets($Connect, 1024)){
        $buffer=$buffer.$Response;
      }
//      echo($buffer);
    }else echo "Koneksi Gagal";
    
    include("parse.php");
    $buffer=Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
    $buffer=explode("\r\n",$buffer);
    $retData = [];
    for($a=0;$a<count($buffer);$a++){
      $data=Parse_Data($buffer[$a],"<Row>","</Row>");
      $PIN=Parse_Data($data,"<PIN>","</PIN>");
      $DateTime=Parse_Data($data,"<DateTime>","</DateTime>");
      $Verified=Parse_Data($data,"<Verified>","</Verified>");
      $Status=Parse_Data($data,"<Status>","</Status>");

      if($PIN != '') {

        array_push($retData, [
          'nik' => $PIN,
          'datetime' => $DateTime,
          'status' => $Status == '0' ? '4' : '5',
        ]);
      }
    }
    return $retData;
  }
  public static function clearData($ip, $key)
  {
    $Connect = fsockopen($ip, "80", $errno, $errstr, 1);
    if($Connect){
      $soap_request="<ClearData><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey><Arg><Value xsi:type=\"xsd:integer\">3</Value></Arg></ClearData>";
      $newLine="\r\n";
      fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
        fputs($Connect, "Content-Type: text/xml".$newLine);
        fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
        fputs($Connect, $soap_request.$newLine);
      $buffer="";
      while($Response=fgets($Connect, 1024)){
        $buffer=$buffer.$Response;
      }
      echo($buffer);
      return "Koneksi Berhasil";
    }
    return "Koneksi Gagal";
    
  }
}
